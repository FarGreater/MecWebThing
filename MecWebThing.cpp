#include <MecWebThing.h>
#include <ElegantOTA.h>
#define WiFI_CONFIG_FILE "/WiFICONFIGFILE"

#if defined(ESP8266)
ESP8266WebServer server(80);
#elif defined(ESP32)
WebServer server(80);
#endif

MecThing::MecThing(String tmpID, String tmpTitle, String tmpDescription, String tmpType[], int tmpTypeCount, MecLogging(*MyLog), MecWifi(*MyWifi))
{

  // max number of networks is 10 (ignores the rest)
  if (tmpTypeCount > 10)
  {
    tmpTypeCount = 10;
  }

  ThingTypeCount = tmpTypeCount;

  // copy's the ssid's and passwords to local array
  for (int n; n < tmpTypeCount; n++)
  {
    ThingType[n] = tmpType[n];
  }

  ID = tmpID;
  Title = tmpTitle;

  //ThingType = tmpType;
  Description = tmpDescription;
  _MyLog = MyLog;
  _MyWifi = MyWifi;
  (*_MyLog).infoNl("MecThing: Created", Lib);
};

MecProperty::MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, String tmpDefaultValue,
                         String tmpSemanticType /*= "*/, bool tmpReadOnly /*= false*/, String tmpType /*= TypeString*/)
{
  _MyLog = MyLog;
  SemanticType = tmpSemanticType;
  ID = tmpID;
  Title = tmpTitle;
  Type = tmpType;
  ReadOnly = tmpReadOnly;
  Description = tmpDescription;
  _stringValue = tmpDefaultValue;

  (*_MyLog).infoNl("MecProperty: Created", Lib);
};

MecProperty::MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, bool tmpDefaultValue,
                         String tmpSemanticType /*= "*/, bool tmpReadOnly /* = false*/, String tmpType /* = TypeBoolean*/)
{
  _MyLog = MyLog;
  SemanticType = tmpSemanticType;
  ID = tmpID;
  Title = tmpTitle;
  Type = tmpType;
  ReadOnly = tmpReadOnly;
  Description = tmpDescription;
  _boolValue = tmpDefaultValue;

  (*_MyLog).infoNl("MecProperty: Created", Lib);
};

MecProperty::MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, float tmpDefaultValue,
                         String tmpSemanticType /*= "*/, bool tmpReadOnly /*= false*/, String tmpUnit /*= ""*/, int tmpMin /*= 0*/, int tmpMax /*= 0*/, String tmpType /*= TypeNumber*/)
{
  _MyLog = MyLog;
  SemanticType = tmpSemanticType;
  ID = tmpID;
  Title = tmpTitle;
  Type = tmpType;
  ReadOnly = tmpReadOnly;
  Description = tmpDescription;
  Unit = tmpUnit;
  Min = tmpMin;
  Max = tmpMax;
  _numberValue = tmpDefaultValue;

  (*_MyLog).infoNl("MecProperty: Created", Lib);
};

MecProperty::MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, int tmpDefaultValue,
                         String tmpSemanticType /*= "*/, bool tmpReadOnly /*= false*/, String tmpUnit /*= ""*/, int tmpMin /*= 0*/, int tmpMax /*= 0*/, String tmpType /*= TypeInteger*/)
{
  _MyLog = MyLog;
  SemanticType = tmpSemanticType;
  ID = tmpID;
  Title = tmpTitle;
  Type = tmpType;
  ReadOnly = tmpReadOnly;
  Description = tmpDescription;
  Unit = tmpUnit;
  Min = tmpMin;
  Max = tmpMax;
  _intValue = tmpDefaultValue;

  (*_MyLog).infoNl("MecProperty: Created", Lib);
};

void MecProperty::SetValue(String newValue, int Source /*= _ChangedByProgram*/)
{
  if (_stringValue != newValue)
  {
    _stringValue = newValue;
    _Changed = Source;
  }
};

void MecProperty::SetValue(bool newValue, int Source /*= _ChangedByProgram*/)
{
  if (_boolValue != newValue)
  {
    _boolValue = newValue;
    _Changed = Source;
  }
};

void MecProperty::SetValue(int newValue, int Source /*= _ChangedByProgram*/)
{
  if (_intValue != newValue)
  {
    _intValue = newValue;
    _Changed = Source;
  }
};

void MecProperty::SetValue(float newValue, int Source /*= _ChangedByProgram*/)
{
  if (_numberValue != newValue)
  {
    _numberValue = newValue;
    _Changed = Source;
  }
};

String MecProperty::GetStringValue()
{
  String StringValue = _stringValue;

  if (Type == TypeBoolean)
  {
    StringValue = String(_boolValue);
  };
  if (Type == TypeInteger)
  {
    StringValue = String(_intValue);
  };
  if (Type == TypeNumber)
  {
    StringValue = String(_numberValue);
  };
  return StringValue;
};

bool MecProperty::GetBooleanValue()
{

  return _boolValue;
};

int MecProperty::GetIntegerValue()
{
  int IntergerValue = _intValue;

  if (Type == TypeNumber)
  {
    IntergerValue = _numberValue;
  };
  return _intValue;
};

float MecProperty::GetNumberValue()
{
  float NumberValue = _numberValue;

  if (Type == TypeInteger)
  {
    NumberValue = _intValue;
  }
  return _numberValue; // er dette ikke en fejl?, skal det ikke være NumberValue????
};

bool MecProperty::IsChanged(int FromSource /*= _ChangedFromRemote*/)
{
  if (_Changed == FromSource)
  {
    _Changed = _NoChange;
    return true;
  }
  else
  {
    return false;
  };
};

int MecProperty::GetIsChangedStatus()
{
  return _Changed;
};

void MecProperty::UpdateValueFromJson(String JsonText)
{
  DynamicJsonDocument JsonDoc(1000);
  DeserializationError err = deserializeJson(JsonDoc, JsonText);
  if (err)
  {
    (*_MyLog).infoNl("Ingen, eller manglende Json i forespørgsel (deserializeJson() failed with code: " + String(err.c_str()) + ")", Lib);
  }
  else
  {
    serializeJsonPretty(JsonDoc, JsonText);

    JsonVariant jvValue = JsonDoc.getMember(ID);
    if (jvValue.isNull())
    {
      (*_MyLog).infoNl("No Json structure (error decoding Json)", Lib);
    }
    else
    {
      (*_MyLog).infoNl("Finding new JsonValue", Lib);
      (*_MyLog).infoNl(JsonText, Lib);

      if (Type == TypeBoolean && jvValue.is<bool>())
      {
        (*_MyLog).infoNl("Value is BOOLEAN", Lib);
        _boolValue = jvValue.as<bool>();
        _Changed = _ChangedFromRemote;
      }
      if (Type == TypeString && jvValue.is<String>())
      {
        (*_MyLog).infoNl("Value is String", Lib);
        _stringValue = jvValue.as<String>();
        _Changed = _ChangedFromRemote;
      }
      if (Type == TypeNumber || Type == TypeInteger)
      {
        if (jvValue.is<int>())
        {
          _intValue = jvValue.as<int>();
          _numberValue = jvValue.as<int>();
        }
        if (jvValue.is<float>())
        {
          _intValue = jvValue.as<float>();
          _numberValue = jvValue.as<float>();
        }
        _Changed = _ChangedFromRemote;
      }
    }
  }
}

String MecProperty::CreateJsonReturnString()
{
  String JsonResultString;

  StaticJsonDocument<200> JsonResult;
  if (Type == TypeBoolean)
  {
    JsonResult[ID] = _boolValue;
  }
  if (Type == TypeInteger)
  {
    JsonResult[ID] = _intValue;
  }
  if (Type == TypeNumber)
  {
    JsonResult[ID] = _numberValue;
  }
  if (Type == TypeString)
  {
    JsonResult[ID] = _stringValue;
  }

  serializeJsonPretty(JsonResult, JsonResultString);
  return JsonResultString;
}

void MecThing::addProperty(MecProperty *tmpMecProperty)
{

  (*_MyLog).infoNl("MecThing::addProperty", Lib);
  if (CountOfPropterties < 10)
  {
    MecProperties[CountOfPropterties] = tmpMecProperty;
    CountOfPropterties++;
  }
  else
  {
    (*_MyLog).infoNl("MecThing::addProperty, YOU CAN ONLY HAVE 10 Propertys, Dropping nr. 11", War);
  }
};

void MecThing::HandlePropertyRequest()
{
  String JsonResultString = "";
  //MecProperty *CurrentProperty; // The Requestet/Changed Property

  (*_MyLog).infoNl("HandlePropertyRequest", Lib);
  (*_MyLog).infoNl("URI: " + String(server.uri()), Lib);

  // Find Requestet/Changed propertyObject.
  for (int i = 0; i < CountOfPropterties; i++)
  {
    if (server.uri().endsWith(MecProperties[i]->ID))
    {
      MecProperties[i]->UpdateValueFromJson(server.arg("plain"));
      JsonResultString = MecProperties[i]->CreateJsonReturnString();
      (*_MyLog).infoNl("Http reguest forespørgsel efter: " + MecProperties[i]->ID + " Type: " + MecProperties[i]->Type, Lib);
    }
  }

  if (JsonResultString == "") // alså hvis vi ikke har fundet den rigtige parameter
  {
    StaticJsonDocument<200> JsonResult;
    for (int i = 0; i < CountOfPropterties; i++)
    {
      if (MecProperties[i]->Type == TypeBoolean)
      {
        JsonResult[MecProperties[i]->ID] = MecProperties[i]->GetBooleanValue();
      }
      if (MecProperties[i]->Type == TypeInteger)
      {
        JsonResult[MecProperties[i]->ID] = MecProperties[i]->GetIntegerValue();
      }
      if (MecProperties[i]->Type == TypeNumber)
      {
        JsonResult[MecProperties[i]->ID] = MecProperties[i]->GetNumberValue();
      }
      if (MecProperties[i]->Type == TypeString)
      {
        JsonResult[MecProperties[i]->ID] = MecProperties[i]->GetStringValue();
      }
    }
    serializeJsonPretty(JsonResult, JsonResultString);
  };

  // Poblish result
  server.send(200, "application/json", JsonResultString);

  (*_MyLog).infoNl("Response:\n" + JsonResultString, Lib);
}

void MecThing::handleNotFound()
{
  (*_MyLog).infoNl("HandleNotFound", Lib);

  String URISti = server.uri();

  if (URISti.startsWith("/properties"))
  {
    (*_MyLog).infoNl("Request for Property data recived", Lib);
    HandlePropertyRequest();
  }
  else // unknown Request
  {
    String message = "File Not Found\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i = 0; i < server.args(); i++)
    {
      message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", message);
    (*_MyLog).infoNl(message, Deb);
  }
}

void MecThing::ShowLog()
{

  (*_MyLog).infoNl("ShowLog", Lib);
  (*_MyLog).infoNl((*_MyWifi).LongWifiStatus() + (*_MyLog).GetLogArray(), Lib);
  server.send(200, "text/plain", (*_MyWifi).LongWifiStatus() + (*_MyLog).GetLogArray());
};

void MecThing::publishJSONdata()
{
  (*_MyLog).infoNl("publishJsonData", Lib);
  (*_MyLog).infoNl(JsonThingDescription, Lib);
  server.send(200, "application/json", JsonThingDescription);
};

void MecThing::CreateJsonDescription()
{
  //StaticJsonDocument<6000> JsonThingDesc;
  DynamicJsonDocument JsonThingDesc(6000);
  JsonObject properties;

  if (ThingTypeCount != 0)
  {
    JsonThingDesc["@context"] = "https://iot.mozilla.org/schemas/";
    /*if (ThingTypeCount == 1)
    {
      JsonThingDesc["@type"] = ThingType[0];
    } else{*/
    JsonArray TType = JsonThingDesc.createNestedArray("@type");
    for (int n; n < ThingTypeCount; n++)
    {
      TType.add(ThingType[n]);
    }
    //}
  }

  //  "@context": "https://iot.mozilla.org/schemas/",
  //  "@type": ["Light", "OnOffSwitch"],
  //JsonThingDesc["@context"] = "https://iot.mozilla.org/schemas/";
  //JsonThingDesc["@type"] = Type;
  //JsonArray TType = JsonThingDesc.createNestedArray("@type");
  // TType.add(ThingType);

  // TType.add("Light");

  //////////////////////

  JsonThingDesc["id"] = ID;
  JsonThingDesc["title"] = Title;
  JsonThingDesc["description"] = Description;

  properties = JsonThingDesc.createNestedObject("properties");

  // loop gennem
  for (int i = 0; i < CountOfPropterties; i++)
  {
    JsonObject jsonPropertyDesc = properties.createNestedObject(MecProperties[i]->ID);
    if (MecProperties[i]->SemanticType != "")
    {
      jsonPropertyDesc["@type"] = MecProperties[i]->SemanticType;
    }

    jsonPropertyDesc["title"] = MecProperties[i]->Title;
    jsonPropertyDesc["type"] = MecProperties[i]->Type;
    jsonPropertyDesc["description"] = MecProperties[i]->Description;
    if (MecProperties[i]->ReadOnly == true)
    {
      jsonPropertyDesc["readOnly"] = MecProperties[i]->ReadOnly;
    }
    if (MecProperties[i]->Unit != "")
    {
      jsonPropertyDesc["Unit"] = MecProperties[i]->Unit;
    }
    //(*_MyLog).infoNl("Min:" + String(MecProperties[i]->Min) + " Max; " + String(MecProperties[i]->Max), Deb);
    if (MecProperties[i]->Min != 0 || MecProperties[i]->Max != 0)
    {
      //(*_MyLog).infoNl("SKAL MEDMin:" + String(MecProperties[i]->Min) + " Max; " + String(MecProperties[i]->Max), Deb);
      jsonPropertyDesc["minimum"] = MecProperties[i]->Min;
      jsonPropertyDesc["maximum"] = MecProperties[i]->Max;
    }
    JsonArray proplinks = jsonPropertyDesc.createNestedArray("links");
    JsonObject hreflinks = proplinks.createNestedObject();

    hreflinks["href"] = "/properties/" + MecProperties[i]->ID;
  }

  // add link section
  //links = JsonThingDesc.createNestedObject("links");
  JsonArray links = JsonThingDesc.createNestedArray("links");
  JsonObject ilinks = links.createNestedObject();

  ilinks["rel"] = "properties";
  ilinks["href"] = "/properties";

  JsonObject iilinks = links.createNestedObject();

  iilinks["rel"] = "alternate";
  iilinks["mediaType"] = "text/html";
  iilinks["href"] = "/web";

  serializeJsonPretty(JsonThingDesc, JsonThingDescription);
  (*_MyLog).infoNl("posting Json description", Deb);
  (*_MyLog).infoNl(JsonThingDescription, Lib);
}

void MecThing::ChangeFromWeb()
{
  String startHTML = "<!DOCTYPE html> <html> <head> <meta charset=\"utf-8\"></head><body> <h1>" + Title + "</h1>" +
                     Description + "<br> <form action =\"/save\" method=\"get\"> <table>";

  String endHTML = "<td> <button>Save...</button> </td></table></form></body></html>";

  String midHTML = "";

  String type = "";
  String value = "";

  for (int i = 0; i < CountOfPropterties; i++)
  {
    value = MecProperties[i]->GetStringValue();

    if (MecProperties[i]->Type == TypeString)
    {
      type = "type=\"text\"";
    };
    if (MecProperties[i]->Type == TypeBoolean)
    {
      type = "type=\"checkbox\"";
      if (MecProperties[i]->GetBooleanValue() == true)
      {
        type = type + " checked ";
      };
    }
    if (MecProperties[i]->Type == TypeNumber)
    {
      type = "type=\"number\" step=\"0.01\"";
      /*"min=\"0\" max=\"23\"*/
    };
    if (MecProperties[i]->Type == TypeInteger)
    {
      type = "type=\"number\"";
      /*"min=\"0\" max=\"23\"*/
    };
    if (MecProperties[i]->Min != 0 || MecProperties[i]->Max != 0)
    {
      type = type + " min=\"" + String(MecProperties[i]->Min) + "\"";
      type = type + " max=\"" + String(MecProperties[i]->Max) + "\"";
    }

    if (MecProperties[i]->ReadOnly == true)
    {
      type = type + " readonly disabled=\"disabled\" ";
    }

    midHTML = midHTML + "<tr><td><p><b>";
    midHTML = midHTML + MecProperties[i]->Title + "</b><br>";
    midHTML = midHTML + MecProperties[i]->Description + "</p></td>";
    midHTML = midHTML + "<td><input " + type;
    midHTML = midHTML + " value=\"" + MecProperties[i]->GetStringValue() + "\" size=40 name=\"" + MecProperties[i]->ID + "\"";
    midHTML = midHTML + "id=\"" + MecProperties[i]->ID + "\"></td></tr>";
  }

  (*_MyLog).infoNl("ChangeFromWeb", Deb);
  (*_MyLog).infoNl(startHTML + endHTML, Lib);
  server.send(200, "text/html", startHTML + midHTML + endHTML);
}

void MecThing::SaveChangesFromWeb()
{
  for (int i = 0; i < CountOfPropterties; i++)
  {
    (*_MyLog).infoNl("Get: " + MecProperties[i]->ID + " result: " + server.arg(MecProperties[i]->ID), Lib);
    if (MecProperties[i]->ReadOnly == false)
    {

      if (MecProperties[i]->Type == TypeString)
      {
        MecProperties[i]->SetValue(String(server.arg(MecProperties[i]->ID)), _ChangedFromRemote);
      };
      if (MecProperties[i]->Type == TypeBoolean)
      {
        if (server.arg(MecProperties[i]->ID) == "")
        {
          MecProperties[i]->SetValue(false, _ChangedFromRemote);
        }
        else
        {
          MecProperties[i]->SetValue(true, _ChangedFromRemote);
        }
      }
      if (MecProperties[i]->Type == TypeNumber)
      {
        MecProperties[i]->SetValue(server.arg(MecProperties[i]->ID).toFloat(), _ChangedFromRemote);
      };
      if (MecProperties[i]->Type == TypeInteger)
      {
        MecProperties[i]->SetValue((int)server.arg(MecProperties[i]->ID).toInt(), _ChangedFromRemote);
      };
    }
  };

  server.send(200, "text/html", "<html> <head> <meta http-equiv=\"refresh\" content=\"4;url=/web\" /></head><body>Data gemt... reloader om 4 sek </body> </html>");
}

void MecThing::ChangeSettings()
{
  String startHTML = "<!DOCTYPE html> <html> <head> <meta charset=\"utf-8\"></head><body> <h1> Change Wifi Settings</h1>" + Title + "<br> Use this page to update settings for Wifi <br> <form action =\"/settingssave\" method=\"get\"> <table>";

  String endHTML = "<td> <button>Save...</button> </td></table></form>";
  endHTML = endHTML + "<form action=\"/settingsformat\"><button>Factory reset</button></form>";
  endHTML = endHTML + "</body></html>";

  String midHTML = "";

  String type = "";
  String value = "";

  // SSID/AP name
  midHTML = midHTML + "<tr><td><p><b>";
  midHTML = midHTML + "SSID & AP name</b><br>";
  midHTML = midHTML + "Navnet på Accespunktet </p></td>";
  midHTML = midHTML + "<td><input type=\"text\"";
  midHTML = midHTML + " value=\"" + (*_MyWifi).mDNSName + "\" size=40 name=\"mDNSName\"";
  midHTML = midHTML + "id=\"mDNSName\"></td></tr>";

  // AP password
  midHTML = midHTML + "<tr><td><p><b>";
  midHTML = midHTML + "AP password</b><br>";
  midHTML = midHTML + "Password til accespunktet </p></td>";
  midHTML = midHTML + "<td><input type=\"text\"";
  midHTML = midHTML + " value=\"********\" size=40 name=\"APPassword\"";
  midHTML = midHTML + "id=\"APPassword\"></td></tr>";

  // AP enabled

  midHTML = midHTML + "<tr><td><p><b>";
  midHTML = midHTML + "AP tænt?</b><br>";
  midHTML = midHTML + "Skal accespunktet være tænt </p></td>";
  midHTML = midHTML + "<td><input type=\"checkbox\"";
  if ((*_MyWifi).RunSoftAP == true)
  {
    midHTML = midHTML + " checked value= true";
  }
  else
  {
    midHTML = midHTML + "value= false";
  }
  midHTML = midHTML + " size=40 name=\"RunSoftAP\"";
  midHTML = midHTML + "id=\"RunSoftAP\"></td></tr>";

  for (int n = 0; n < (*_MyWifi).WifiCount; n++)
  {

    //   WiFiSSID[n] = WiFiSSID[n];

    // SSID/AP name
    midHTML = midHTML + "<tr><td><p><b>";
    midHTML = midHTML + n + ": WiFi Navn</b><br>";
    midHTML = midHTML + "Navnet på Wifi </p></td>";
    midHTML = midHTML + "<td><input type=\"text\"";
    midHTML = midHTML + " value=\"" + (*_MyWifi).WiFiSSID[n] + "\" size=40 name=\"WiFiSSID" + n + "\"";
    midHTML = midHTML + "id=\"WiFiSSID" + n + "\"></td></tr>";

    //   WiFiPassword[n] = tWiFiPassword[n];

    // AP password
    midHTML = midHTML + "<tr><td><p><b>";
    midHTML = midHTML + n + ": WiFi password</b><br>";
    midHTML = midHTML + "Password til wifi</p></td>";
    midHTML = midHTML + "<td><input type=\"text\"";
    midHTML = midHTML + " value=\"********\" size=40 name=\"WiFiPassword" + n + "\"";
    midHTML = midHTML + "id=\"WiFiPassword" + n + "\"></td></tr>";
  }

  if ((*_MyWifi).WifiCount < 10)
  {
    // SSID/AP name
    midHTML = midHTML + "<tr><td><p><b>";
    midHTML = midHTML + (*_MyWifi).WifiCount + ": WiFi Navn</b><br>";
    midHTML = midHTML + "Navnet på Wifi </p></td>";
    midHTML = midHTML + "<td><input type=\"text\"";
    midHTML = midHTML + " value=\"\" size=40 name=\"WiFiSSID" + (*_MyWifi).WifiCount + "\"";
    midHTML = midHTML + "id=\"WiFiSSID" + (*_MyWifi).WifiCount + "\"></td></tr>";

    //   WiFiPassword[n] = tWiFiPassword[n];

    // AP password
    midHTML = midHTML + "<tr><td><p><b>";
    midHTML = midHTML + (*_MyWifi).WifiCount + ": WiFi password</b><br>";
    midHTML = midHTML + "Password til wifi</p></td>";
    midHTML = midHTML + "<td><input type=\"text\"";
    midHTML = midHTML + " value=\"********\" size=40 name=\"WiFiPassword" + (*_MyWifi).WifiCount + "\"";
    midHTML = midHTML + "id=\"WiFiPassword" + (*_MyWifi).WifiCount + "\"></td></tr>";
  }

  (*_MyLog).infoNl("ChangeFromWeb", Deb);
  (*_MyLog).infoNl(startHTML + endHTML, Lib);
  server.send(200, "text/html", startHTML + midHTML + endHTML);
}

void MecThing::SaveChangeSettings()
{
  if (SPIFFS.exists(WiFI_CONFIG_FILE))
  {
    // Delete if we want to re-calibrate
    (*_MyLog).infoNl("Thing:SPIFFS: Deleting WiFI_CONFIG_FILE file", Inf);
    SPIFFS.remove(WiFI_CONFIG_FILE);
  }

  // store data
  File f = SPIFFS.open(WiFI_CONFIG_FILE, "w");
  if (f)
  {
    (*_MyLog).infoNl("Get: mDNSName result: " + server.arg("mDNSName"), Inf);
    f.print(server.arg("mDNSName") + '\n');
    //    f.write(server.arg("mDNSName"));
    (*_MyLog).infoNl("Get: APPassword result: " + server.arg("APPassword"), Inf);
    if (String(server.arg("APPassword")) == "********")
    {
      f.print((*_MyWifi).APPassword + '\n');
    }
    else
    {
      f.print(server.arg("APPassword") + '\n');
    }

    (*_MyLog).infoNl("Get: RunSoftAP result: \"" + server.arg("RunSoftAP") + "\"", Inf);
    if (String(server.arg("RunSoftAP")) != "true")
    {
      f.print("false\n");
      (*_MyLog).infoNl("Get: FALSE", Inf);
    }
    else
    {
      f.print("true\n");
      (*_MyLog).infoNl("Get: FALSE", Inf);
    }

    for (int i = 0; i < 10; i++)
    {
      if (String(server.arg("WiFiSSID" + String(i))) != "")
      {
        (*_MyLog).infoNl("Get: WiFiSSID1" + String(i) + " result: " + server.arg("WiFiSSID" + String(i) + ""), Inf);
        f.print(server.arg("WiFiSSID" + String(i)) + "\n");
        if (String(server.arg("WiFiPassword" + String(i))) == "********")
        {
          (*_MyLog).infoNl("Get: WiFiPassword" + String(i) + " result: " + (*_MyWifi).WiFiPassword[i], Inf);
          f.print((*_MyWifi).WiFiPassword[i] + "\n");
        }
        else
        {
          (*_MyLog).infoNl("Get: WiFiPassword" + String(i) + " result: " + server.arg("WiFiPassword" + String(i)), Inf);
          f.print(server.arg("WiFiPassword" + String(i)) + "\n");
        }
      }
    }
    f.close();
  }

  f = SPIFFS.open(WiFI_CONFIG_FILE, "r");
  char buffer[64];

  while (f.available())
  {
    int l = f.readBytesUntil('\n', buffer, sizeof(buffer));
    buffer[l] = 0;
    (*_MyLog).infoNl(buffer, Inf);
  }
  //if (f.readBytes((char *)calData, 14) == 14)
  //  calDataOK = 1;
  f.close();

#if defined(ESP8266)
  server.send(200, "text/html", "<html> <head> <meta http-equiv=\"refresh\" content=\"20;url=/settings\" /></head><body>Data gemt... reloader om 20 sekunder, <br><br><b>LAV MANUAL RESET FOR AT SE OG BRUGE ÆNDRINGER</body> </html>");

#elif defined(ESP32)
  server.send(200, "text/html", "<html> <head> <meta http-equiv=\"refresh\" content=\"10;url=/settings\" /></head><body>Reloader om 10 sekunder, <br><br><b>Data gemt... </body> </html>");

  ESP.restart();
// ESP.reset();
// String reset = String(1/0);
#endif
}

void MecThing::FormatChangeSettings()
{
  if (SPIFFS.exists(WiFI_CONFIG_FILE))
  {
    (*_MyLog).infoNl("Thing:SPIFFS: Formating (deliting) drive and files", Inf);
    (*_MyLog).infoNl("SPIFFS: Formating file system", Inf);
    bool formatted = SPIFFS.format();
    if (formatted)
    {
      (*_MyLog).infoNl("SPIFFS: Success formatting", Inf);
    }
    else
    {
      (*_MyLog).infoNl("SPIFFS: Error formatting", Inf);
    }
    SPIFFS.begin();
  }

#if defined(ESP8266)
  server.send(200, "text/html", "<html> <head> <meta http-equiv=\"refresh\" content=\"20;url=/settings\" /></head><body> Sletter alle data, og nulstiler til fabriks indstillinger<br><br><b>LAV MANUAL RESET FOR ÆNDRINGEN TRÆDER I KRAFT</body> </html>");

#elif defined(ESP32)
  server.send(200, "text/html", "<html> <head> <meta http-equiv=\"refresh\" content=\"10;url=/settings\" /></head><body> Reloader om 10 sekunder<br><br><b>Sletter alle data, og nulstiler til fabriks indstillinger</body> </html>");
  ESP.restart();
// ESP.reset();
// String reset = String(1/0);
#endif
}

void MecThing::setup()
{
  CreateJsonDescription();

  server.on("/", std::bind(&MecThing::publishJSONdata, this));
  server.on("/log", std::bind(&MecThing::ShowLog, this));
  server.on("/web", std::bind(&MecThing::ChangeFromWeb, this));
  server.on("/save", std::bind(&MecThing::SaveChangesFromWeb, this));
  server.on("/settings", std::bind(&MecThing::ChangeSettings, this));
  server.on("/settingssave", std::bind(&MecThing::SaveChangeSettings, this));
  server.on("/settingsformat", std::bind(&MecThing::FormatChangeSettings, this));

  server.onNotFound(std::bind(&MecThing::handleNotFound, this));

  ElegantOTA.setID(Title.c_str());
  ElegantOTA.begin(&server);

  server.begin();
};

void MecThing::loop()
{
  server.handleClient();
};
