/**
 * ****************************************************************************
 * 
 * * WebThingsSonoff
 * 
 * Libary for WebThings communication for the ESP2866
 * ****************************************************************************
 * By Michael Egtved Christensen 2019 ->
 * 
 * NOTE:
 * ? For usage see reference implementation
 * 
 * Json:
 *      https://arduinojson.org/v6/example/generator/
 *      https://randomnerdtutorials.com/decoding-and-encoding-json-with-arduino-or-esp8266/
 *
 * 
*/
#ifndef MecWebThing_h
#define MecWebThing_h

#include <Arduino.h>
#include <MecLogging.h>
#include <MecWifi.h>

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#elif defined(ESP32)
#include <WiFi.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#endif

//#include <ElegantOTA.h>
#include <ArduinoJson.h>

// Årsagen til ændringer
#define _ChangedFromRemote -1
#define _NoChange 0
#define _ChangedByProgram 1

#define TypeString "string"
#define TypeBoolean "boolean"
#define TypeNumber "number"
#define TypeInteger "integer"

class MecProperty
{
public:
  MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, String tmpDefaultValue,
              String tmpSemanticType = "", bool tmpReadOnly = false, String tmpType = TypeString);
  MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, bool tmpDefaultValue,
              String tmpSemanticType = "", bool tmpReadOnly = false, String tmpType = TypeBoolean);
  MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, int tmpDefaultValue,
              String tmpSemanticType = "", bool tmpReadOnly = false, String tmpUnit = "", int tmpMin = 0, int tmpMax = 0, String tmpType = TypeInteger);
  MecProperty(MecLogging(*MyLog), String tmpID, String tmpTitle, String tmpDescription, float tmpDefaultValue,
              String tmpSemanticType = "", bool tmpReadOnly = false, String tmpUnit = "", int tmpMin = 0, int tmpMax = 0, String tmpType = TypeNumber);

  bool IsChanged(int FromSource = _ChangedFromRemote);
  int GetIsChangedStatus();

  void SetValue(String newValue, int Source = _ChangedByProgram);
  void SetValue(bool newValue, int Source = _ChangedByProgram);
  void SetValue(int newValue, int Source = _ChangedByProgram);
  void SetValue(float newValue, int Source = _ChangedByProgram);

  String GetStringValue();
  bool GetBooleanValue();
  int GetIntegerValue();
  float GetNumberValue();

  void UpdateValueFromJson(String JsonText);
  String CreateJsonReturnString();

  String SemanticType;
  String ID;
  String Title;
  String Type;
  String Description;
  boolean ReadOnly;
  String Unit;
  int Min = 0;
  int Max = 0;

private:
  String _stringValue = "Default string value";
  bool _boolValue = false;
  int _intValue = 42;
  float _numberValue = 3.14;

  int _Changed;
  MecLogging *_MyLog;
};

class MecThing
{
public:
  MecThing(String tmpID, String tmpTitle, String tmpDescription, String tmpType[], int tmpTypeCount, MecLogging(*MyLog), MecWifi(*MyWifi));

  void addProperty(MecProperty *tmpMecProperty);
  void publishJSONdata();
  void ShowLog();
  void handleNotFound();
  void HandlePropertyRequest();
  void ChangeFromWeb(); // create webpage for manualy updating...
  void SaveChangesFromWeb();
  void ChangeSettings();
  void SaveChangeSettings();
  void FormatChangeSettings();
  void setup();
  void loop();

  MecProperty *MecProperties[10];
  int CountOfPropterties = 0; // Number of MecNodes

#if defined(ESP8266)
  ESP8266WebServer server;
#elif defined(ESP32)
  WebServer server;
#endif

  String Title;

private:
  String ID;

  String Description;
  String ThingType[10];
  int ThingTypeCount;
  MecLogging *_MyLog;
  MecWifi *_MyWifi;
  void CreateJsonDescription();
  String JsonThingDescription;
};

#endif
